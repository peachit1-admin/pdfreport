﻿using PdfSharpCore;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;
using PdfSharpCore.Pdf.IO;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VetCV.HtmlRendererCore.PdfSharpCore;

namespace ConsoleApp1
{
    class PdfSharp
    {
        public void run(List<StringBuilder> htmlList)
        {
            PdfDocument document = new PdfDocument();

            for (var i = 0; i < htmlList.Count; i++)
            {
                var html = htmlList[i];
                PdfGenerateConfig config = new PdfGenerateConfig();
                config.PageOrientation = PageOrientation.Landscape;
                config.PageSize = PageSize.A4;

                PdfDocument doc = PdfGenerator.GeneratePdf(html.ToString(), config);
                var path = $"./temp/{doc.Guid}.pdf";
                doc.Save(path);

                PdfDocument pdfDoc = PdfReader.Open(path, PdfDocumentOpenMode.Import);

                // Add link              
                if (i == 1)
                {
                    PdfPage page = document.AddPage(pdfDoc.Pages[0]);
                    XGraphics gfx = XGraphics.FromPdfPage(page);

                    var xrect1 = new XRect(465, 412, 160, 12);
                    var rect1 = gfx.Transformer.WorldToDefaultPage(xrect1);
                    var pdfrect1 = new PdfRectangle(rect1);
                    //gfx.DrawRectangle(new XPen(XColors.Blue), xrect1);
                    page.AddWebLink(pdfrect1, "https://www.cyberhealth.com.au");
                }
                else if (i == 3)
                {
                    PdfPage page = document.AddPage(pdfDoc.Pages[0]);
                    XGraphics gfx = XGraphics.FromPdfPage(page);

                    var xrect1 = new XRect(819, 326, 14, 14);
                    var rect1 = gfx.Transformer.WorldToDefaultPage(xrect1);
                    var pdfrect1 = new PdfRectangle(rect1);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect1);
                    page.AddWebLink(pdfrect1, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect2 = new XRect(819, 416, 14, 14);
                    var rect2 = gfx.Transformer.WorldToDefaultPage(xrect2);
                    var pdfrect2 = new PdfRectangle(rect2);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect2);
                    page.AddWebLink(pdfrect2, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect3 = new XRect(819, 506, 14, 14);
                    var rect3 = gfx.Transformer.WorldToDefaultPage(xrect3);
                    var pdfrect3 = new PdfRectangle(rect3);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect3);
                    page.AddWebLink(pdfrect3, "https://www.cyberhealth.com.au/contact-us/");
                }
                else if (i == 4)
                {
                    PdfPage page = document.AddPage(pdfDoc.Pages[0]);
                    XGraphics gfx = XGraphics.FromPdfPage(page);

                    var xrect1 = new XRect(819, 116, 14, 14);
                    var rect1 = gfx.Transformer.WorldToDefaultPage(xrect1);
                    var pdfrect1 = new PdfRectangle(rect1);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect1);
                    page.AddWebLink(pdfrect1, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect2 = new XRect(819, 205, 14, 14);
                    var rect2 = gfx.Transformer.WorldToDefaultPage(xrect2);
                    var pdfrect2 = new PdfRectangle(rect2);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect2);
                    page.AddWebLink(pdfrect2, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect3 = new XRect(819, 299, 14, 14);
                    var rect3 = gfx.Transformer.WorldToDefaultPage(xrect3);
                    var pdfrect3 = new PdfRectangle(rect3);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect3);
                    page.AddWebLink(pdfrect3, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect4 = new XRect(819, 393, 14, 14);
                    var rect4 = gfx.Transformer.WorldToDefaultPage(xrect4);
                    var pdfrect4 = new PdfRectangle(rect4);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect4);
                    page.AddWebLink(pdfrect4, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect5 = new XRect(819, 487, 14, 14);
                    var rect5 = gfx.Transformer.WorldToDefaultPage(xrect5);
                    var pdfrect5 = new PdfRectangle(rect5);
                    // gfx.DrawRectangle(new XPen(XColors.Red), xrect5);
                    page.AddWebLink(pdfrect5, "https://www.cyberhealth.com.au/contact-us/");
                }
                else if (i == 11)
                {
                    PdfPage page = document.AddPage(pdfDoc.Pages[0]);
                    XGraphics gfx = XGraphics.FromPdfPage(page);

                    var xrect1 = new XRect(479, 415, 161, 15);
                    var rect1 = gfx.Transformer.WorldToDefaultPage(xrect1);
                    var pdfrect1 = new PdfRectangle(rect1);
                    gfx.DrawRectangle(new XPen(XColors.Blue), xrect1);
                    page.AddWebLink(pdfrect1, "https://www.cyberhealth.com.au");

                    var xrect2 = new XRect(451, 451, 120, 34);
                    var rect2 = gfx.Transformer.WorldToDefaultPage(xrect2);
                    var pdfrect2 = new PdfRectangle(rect2);
                    gfx.DrawRectangle(new XPen(XColors.Green), xrect2);
                    page.AddWebLink(pdfrect2, "https://www.cyberhealth.com.au/contact-us/");

                    var xrect3 = new XRect(450, 230, 29, 13);
                    var rect3 = gfx.Transformer.WorldToDefaultPage(xrect3);
                    var pdfrect3 = new PdfRectangle(rect3);
                    gfx.DrawRectangle(new XPen(XColors.Red), xrect3);
                    page.AddWebLink(pdfrect3, "https://www.cyberhealth.com.au/contact-us/");
                }
                else
                {
                    document.AddPage(pdfDoc.Pages[0]);
                }

                File.Delete(path);
            }
            document.Save("./output/Insights-Report.pdf");
        }
    }
}

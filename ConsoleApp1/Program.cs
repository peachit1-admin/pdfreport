﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var company = "PeachIT";
            var score = 75.75m;

            var surveyCategoryModels = new List<SurveyCategoryModel> { };
            surveyCategoryModels.Add(new SurveyCategoryModel { Name = "Risk", Score = 25.75m, IsActive = true });
            surveyCategoryModels.Add(new SurveyCategoryModel { Name = "Communications", Score = 50m, IsActive = true });
            surveyCategoryModels.Add(new SurveyCategoryModel { Name = "Productivity", Score = 0m, IsActive = false });
            surveyCategoryModels.Add(new SurveyCategoryModel { Name = "Cloud", Score = 0m, IsActive = false });

            var surveyReportModel = new SurveyReportModel
            {
                SurveyScore = score,
                CompanyName = company,
                Categories = surveyCategoryModels
            };

            var mainScore = new ScoreModel(
                surveyReportModel.SurveyScore,
                Math.Round(surveyReportModel.SurveyScore),
                Math.Round(surveyReportModel.SurveyScore / 5.0m) * 5,
                $"bar{((Math.Round(surveyReportModel.SurveyScore / 5.0m) * 5) / 5) + 1}"
            );

            var subScoreList = new List<ScoreModel> { };
            foreach (var category in surveyReportModel.Categories)
            {
                var scoreModel = new ScoreModel(
                    category.Score,
                    Math.Round(category.Score),
                    Math.Round(category.Score / 5.0m) * 5,
                    null
                );
                scoreModel.Image = (category.IsActive)
                    ? $"bar{((Math.Round(category.Score / 5.0m) * 5) / 5) + 1}"
                    : $"bar-disabled";
                subScoreList.Add(scoreModel);
            }

            var message = "";
            if (mainScore.ScoreRound < 60) { message = "This score is cause for concern"; }
            else if(mainScore.ScoreRound < 80) { message = "You have room for improvement"; }
            else { message = "You are performing well. Remain diligent!"; }

            StringBuilder first = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/first-page.html"));
            StringBuilder second = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/second-page.html"));
            StringBuilder cover = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cover.html"));
            StringBuilder fourth = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/fourth-page.html"));
            StringBuilder intro = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-intro.html"));
            StringBuilder summary = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-summary.html"));
            StringBuilder cyberRisk = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cyber-risk.html"));            

            StringBuilder cyberRisk3 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cyber-risk-2.html"));
            StringBuilder cyberRisk2 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cyber-risk-3.html"));

            StringBuilder cyberRisk4 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cyber-risk-4.html"));
            StringBuilder cyberRisk5 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-cyber-risk-5.html"));

            StringBuilder securityMain = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-security-main.html"));            
            StringBuilder securitySub1 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-security-sub-1.html"));
            StringBuilder securitySub2 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-security-sub-2.html"));
            StringBuilder securitySub3 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-security-sub-3.html"));
            StringBuilder securitySub4 = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-security-sub-4.html"));
            StringBuilder final = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/report-final.html"));
            StringBuilder last = new StringBuilder(System.IO.File.ReadAllText("./assets/pages-4/last-page.html"));

            summary.Replace("_score", mainScore.ScoreRound.ToString());
            summary.Replace("_company", surveyReportModel.CompanyName);
            summary.Replace("_message", message);
            summary.Replace("_bar", mainScore.Image);

            summary.Replace("_subBar1", subScoreList[0].Image);
            summary.Replace("_subBar2", subScoreList[1].Image);
            summary.Replace("_subBar3", subScoreList[2].Image);
            summary.Replace("_subBar4", subScoreList[3].Image);

            summary.Replace("_module1", surveyReportModel.Categories[0].Name);
            summary.Replace("_module2", surveyReportModel.Categories[1].Name);
            summary.Replace("_module3", surveyReportModel.Categories[2].Name);
            summary.Replace("_module4", surveyReportModel.Categories[3].Name);

            summary.Replace("_colorModel1", surveyReportModel.Categories[0].IsActive ? "#fc0b03" : "#cbcbcb");
            summary.Replace("_colorModel2", surveyReportModel.Categories[1].IsActive ? "#fc0b03" : "#cbcbcb");
            summary.Replace("_colorModel3", surveyReportModel.Categories[2].IsActive ? "#fc0b03" : "#cbcbcb");
            summary.Replace("_colorModel4", surveyReportModel.Categories[3].IsActive ? "#fc0b03" : "#cbcbcb");

            securityMain.Replace("_score", mainScore.ScoreRound.ToString());
            securityMain.Replace("_bar", mainScore.Image);

            cyberRisk.Replace("_score", mainScore.ScoreRound.ToString());
            cyberRisk.Replace("_bar", mainScore.Image);

            var htmlList = new List<StringBuilder>() {
                first, second, cover, fourth, intro, summary, cyberRisk, cyberRisk2, cyberRisk3, cyberRisk4, cyberRisk5, securityMain, securitySub1, securitySub2, securitySub3, securitySub4, final, last };
            new PdfSharp().run(htmlList);
        }

        private class ScoreModel 
        {
            public decimal Score { get; set; }
            public decimal ScoreRound { get; set; }
            public decimal ScoreRound5 { get; set; }
            public string Image { get; set; }

            public ScoreModel(decimal score, decimal scoreRound, decimal scoreRound5, string image)
            {
                Score = score;
                ScoreRound = scoreRound;
                ScoreRound5 = scoreRound5;
                Image = image;
            }
        }

        private class SurveyReportModel
        {
            public decimal SurveyScore { get; set; }
            public string CompanyName { get; set; }
            public List<SurveyCategoryModel> Categories { get; set; }
        }

        private class SurveyCategoryModel
        {
            public string Name { get; set; }
            public decimal Score { get; set; }
            public bool IsActive { get; set; }
        }
    }
}
